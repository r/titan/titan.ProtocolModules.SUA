///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2019 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:		SUA_EncDec.cc
//  Rev:                R4B
//  Prodnr:             CNL 113 478
//  Updated:            2007-07-13
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
#include "SUA_Types.hh"

namespace SUA__Types {

TTCN_Module SUA__EncDec("SUA_EncDec", __DATE__, __TIME__);


OCTETSTRING f__enc__PDU__SUA(const PDU__SUA& pdu)
{
    if (TTCN_Logger::log_this_event(TTCN_DEBUG)) {
	TTCN_Logger::begin_event(TTCN_DEBUG);
	TTCN_Logger::log_event("Encoding PDU_SUA: ");
	pdu.log();
	TTCN_Logger::end_event();
    }
    
    TTCN_Buffer buf;
    pdu.encode(PDU__SUA_descr_, buf, TTCN_EncDec::CT_RAW);
    OCTETSTRING ret_val(buf.get_len(), buf.get_data());
    
    if (TTCN_Logger::log_this_event(TTCN_DEBUG)) {
	TTCN_Logger::begin_event(TTCN_DEBUG);
	TTCN_Logger::log_event("PDU_SUA after encoding: ");
	ret_val.log();
	TTCN_Logger::end_event();
    }
    return ret_val;
}


PDU__SUA f__dec__PDU__SUA(const OCTETSTRING& stream)
{
    if (TTCN_Logger::log_this_event(TTCN_DEBUG)) {
	TTCN_Logger::begin_event(TTCN_DEBUG);
	TTCN_Logger::log_event("Decoding PDU_SUA: ");
	stream.log();
	TTCN_Logger::end_event();
    }

    TTCN_Buffer buf;
    buf.put_os(stream);
    PDU__SUA ret_val;
    ret_val.decode(PDU__SUA_descr_, buf, TTCN_EncDec::CT_RAW);

    if (TTCN_Logger::log_this_event(TTCN_DEBUG)) {
	TTCN_Logger::begin_event(TTCN_DEBUG);
	TTCN_Logger::log_event("Decoded PDU_SUA: ");
	ret_val.log();
	TTCN_Logger::end_event();
    }
    return ret_val;
}

}//namespace
